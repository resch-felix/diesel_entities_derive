extern crate proc_macro;
use proc_macro::TokenStream;

use quote::{format_ident, quote};

#[proc_macro_derive(DbHolder, attributes(db_field, db_type))]
pub fn db_holder_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    impl_db_holder(&ast)
}

fn impl_db_holder(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let db_field = format_ident!(
        "{}",
        &ast.attrs
            .iter()
            .find(|attr| attr.path.is_ident("db_field"))
            .map(|attr| { attr.tokens.to_string() })
            .unwrap_or("db".to_string())
    );

    let db_type = &ast
        .attrs
        .iter()
        .find(|attr| attr.path.is_ident("db_type"))
        .map(|attr| {
            let lit: syn::Ident = attr.parse_args().unwrap();
            lit
        })
        .unwrap_or_else(|| format_ident!("PgConnection"));

    let gen = quote! {
        impl<'a> ::diesel_entities::DbHolder<'a, #db_type> for #name<'a> {
            fn database(&self) -> &'a #db_type {
                self.#db_field
            }
        }
    };

    gen.into()
}

#[proc_macro_derive(IdFor, attributes(id_field, record_type))]
pub fn id_for_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    impl_id_for(&ast)
}

fn impl_id_for(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let db_field = format_ident!(
        "{}",
        &ast.attrs
            .iter()
            .find(|attr| attr.path.is_ident("id_field"))
            .map(|attr| { attr.tokens.to_string() })
            .unwrap_or("id".to_string())
    );

    let record_type = &ast
        .attrs
        .iter()
        .find(|attr| attr.path.is_ident("record_type"))
        .map(|attr| {
            let lit: syn::Ident = attr.parse_args().unwrap();
            lit
        })
        .unwrap_or_else(|| format_ident!("{}", name.to_string()[4..]));

    let gen = quote! {
        impl ::diesel_entities::IdFor<#record_type> for #name<'_> {
            fn record_id(&self) -> <&#record_type as ::diesel::Identifiable>::Id {
                &self.#db_field
            }
        }
    };

    gen.into()
}

#[proc_macro_derive(
    Record,
    attributes(id_field, record_type, db_field, db_type, column_name, table_name)
)]
pub fn record_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    impl_record(&ast)
}

fn impl_record(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let code = quote! {
        impl ::diesel_entities::Record for #name {}
    };

    code.into()
}

#[proc_macro_derive(LazyEntity, attributes(record_type, db_type, column_name, table_name))]
pub fn lazy_entity_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    impl_lazy_entity(&ast)
}

fn impl_lazy_entity(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let record_type = &ast
        .attrs
        .iter()
        .find(|attr| attr.path.is_ident("record_type"))
        .map(|attr| {
            let lit: syn::Ident = attr.parse_args().unwrap();
            lit
        })
        .unwrap_or_else(|| format_ident!("{}", name.to_string()[4..].to_string()));

    let table_name = &ast
        .attrs
        .iter()
        .find(|attr| attr.path.is_ident("table_name"))
        .map(|attr| {
            let lit: syn::Ident = attr.parse_args().unwrap();
            lit
        })
        .unwrap_or_else(|| format_ident!("{}", record_type.to_string().to_lowercase()));

    let db_type = &ast
        .attrs
        .iter()
        .find(|attr| attr.path.is_ident("db_type"))
        .map(|attr| {
            let lit: syn::Ident = attr.parse_args().unwrap();
            lit
        })
        .unwrap_or_else(|| format_ident!("PgConnection"));

    let data: &syn::Data = &ast.data;

    let mut code = quote! {};

    if let syn::Data::Struct(data) = data {
        let fields: &syn::Fields = &data.fields;

        for field in fields {
            if let Some(attr) = field
                .attrs
                .iter()
                .find(|attr| attr.path.is_ident("column_name"))
            {
                let column_name: syn::Ident = attr.parse_args().unwrap();
                let field_name = field.ident.as_ref().unwrap();

                code.extend(quote! {
                    let _query = if let Some(#field_name) = self.#field_name {
                        _query.filter(#table_name::#column_name.eq(#field_name))
                    } else {
                        _query
                    };
                })
            }
        }
    }

    let gen = quote! {
        impl<'d> ::diesel_entities::LazyEntity<'d, #record_type, #db_type> for #name<'d> {
            fn resolve(&self) -> ::diesel::QueryResult<::diesel_entities::Entity<'d, #record_type, #db_type>> {
                use ::diesel_entities::{IdFor, DbHolder};
                use ::diesel::prelude::*;

                let _query = <#record_type as ::diesel::associations::HasTable>::table().find(self.record_id()).into_boxed();

                #code

                ::diesel_entities::Entity::map_result(_query.get_result(self.database()), self.database())
            }
        }
    };

    gen.into()
}
